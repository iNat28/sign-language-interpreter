/*
The point behind this class is to deal with frame syncing. meaning, having a result every frame is too much.
with this class, we implement a way for there to be an accumulative result over a number of frames that is
changeable by the user.
*/
class FramesManager 
{
	currentFrameCount = 0;
	mostCommonShape = undefined;
	frameLimit = 0;

	constructor(frameLimit)
	{
		this.shapeCounts = {};

		this.frameLimit = frameLimit;
	}

	//reset the counting of frames and the amount of each result.
	reset()
	{
		this.shapeCounts = {};
		this.currentFrameCount = 0;
		this.mostCommonShape = undefined;
	}

	//adding a result to the count
	addShape(shapeName)
	{
		if(shapeName in this.shapeCounts)
		{
			this.shapeCounts[shapeName]++;
		}
		else
		{
			this.shapeCounts[shapeName] = 0;
		}

		if(	this.shapeCounts[shapeName] > this.shapeCounts[this.mostCommonShape] ||
			this.mostCommonShape === undefined)
		{
			this.mostCommonShape = shapeName;
		}

		this.currentFrameCount++;
	}
}