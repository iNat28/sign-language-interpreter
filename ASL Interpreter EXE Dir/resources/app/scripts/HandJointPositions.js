//This function returns if a specific joints is one that can be NULL.
function isUndefinedJoint(joint)
{
	return (
		joint === JOINTS_25.INDEX_METACARPAL 	|| 
		joint === JOINTS_25.MIDDLE_METACARPAL 	|| 
		joint === JOINTS_25.RING_METACARPAL 	|| 
		joint === JOINTS_25.LITTLE_METACARPAL
	);
}

/*
Here we have a dictionary of sorts where the key is an axis and the value is a function.
The point of each function is to get the slope of certain positions compared to that axis.
*/
const getHandSlope = {
	X: function(positionsMatrix) {
		return Vector.slope(
			positionsMatrix[JOINTS_21.MIDDLE_PHALANX_PROXIMAL], 
			positionsMatrix[JOINTS_21.RING_PHALANX_PROXIMAL], 
			AXIS.Y, 
			AXIS.Z
		)
	},

	Y: function(positionsMatrix) {
		return Vector.slope(
			positionsMatrix[JOINTS_21.MIDDLE_PHALANX_PROXIMAL],
			positionsMatrix[JOINTS_21.RING_PHALANX_PROXIMAL], 
			AXIS.X, 
			AXIS.Z
		)
	},

	Z: function(positionsMatrix) {
		return Vector.slope(
			positionsMatrix[JOINTS_21.MIDDLE_PHALANX_PROXIMAL],
			Vector.origin,
			AXIS.Y, 
			AXIS.X
		);
	}
}

/*
This function gets the length of the hand.
The length of the hand is determined by the distance of the index finger base from the wrist.
No matter what position the hand is in, this distance never changes.
*/
function getHandLength(positionsMatrix)
{
	return Math.sqrt(Vector.distanceSquared(positionsMatrix[JOINTS_25.INDEX_METACARPAL], Vector.origin));
}

//This is a default hand position that we compare the user's hand to in order to rotate the user's hand.
class DefaultJointPositions {
	jointPositions = undefined;
	handDistance = undefined;
	getSlope = undefined;
	
	constructor(inputJointPositions)
	{
		//here we take the hand position that we got from the camera and convert in from 25 points to 21 points.
		//the extra 4 points are null and we cannot multiply or rotate null points
		this.jointPositions = [];

		for(let i = 0; i < inputJointPositions.length; i++)
		{
			if(!isUndefinedJoint(i))
			{
				this.jointPositions.push(inputJointPositions[i]);
			}
		}

		this.handDistance = getHandLength(this.jointPositions);
		this.getSlope = {
			X: getHandSlope.X(this.jointPositions),
			Y: getHandSlope.Y(this.jointPositions),
			Z: getHandSlope.Z(this.jointPositions)
		}
	}
}
const defaultJointPositions = new DefaultJointPositions(shapes.letters.a.jointPositions);

/*
This function rotates joint positions on a certain axis by multiplying matrices by a certain angle.
*/
function rotateJointPositionsOnAxis(positionsMatrix, axis) 
{
	return Matrix.multiply.byMatrix(
		positionsMatrix,
		Matrix.rotationMatrix[axis](
			getHandSlope[axis](positionsMatrix) - defaultJointPositions.getSlope[axis]
		)
	);
}

/*
This function acts as an additional rotation in order to be safe.
There is a possibility that the rotation isnt perfect with the regular algorithm.
The irregularities are similar so we have this function to take care of them.
*/
function flipJointPositions(jointPositions)
{
	const vector = [1, 1, 1];
	let isVectorOriginal = true;

	if(0 > jointPositions[JOINTS_21.INDEX_PHALANX_TIP][AXIS.X])
	{
		vector[AXIS.X] = -1;
		isVectorOriginal = false;
	}

	if(0 < jointPositions[JOINTS_21.THUMB_METACARPAL][AXIS.Z])
	{
		vector[AXIS.Z] = -1;
		isVectorOriginal = false;
	}

	//was anything changed? if not, we return the same hand position as before.
	if(isVectorOriginal)
	{
		return jointPositions;
	}

	return Matrix.multiply.byVector(jointPositions, vector);
}

/*
this function is the function that runs the entire rotation.
*/
function normalizeHandJointPositions(jointPositions)
{
	//here we move our entire hand according to the wrist. we will put the wrist at (0,0,0) 
	//and all of the other points are moved by the same amount
	jointPositions = Matrix.subtract.byVector(
		jointPositions, 
		jointPositions[JOINTS_25.WRIST]
	);

	//rotating around the z axis first
	jointPositions = rotateJointPositionsOnAxis(jointPositions, 'Z');
	
	//rotating around the x axis
	jointPositions = rotateJointPositionsOnAxis(jointPositions, 'X');

	//rotating around the y axis
	jointPositions = rotateJointPositionsOnAxis(jointPositions, 'Y');

	//rotating around the z axis again (this is for accuracy)
	jointPositions = rotateJointPositionsOnAxis(jointPositions, 'Z');

	//here we reshape the hand positions to a default size.
	jointPositions = Matrix.multiply.byScaler(
		jointPositions, 
		defaultJointPositions.handDistance / getHandLength(jointPositions)
	);

	//this are for dealing with specific cases where the rotation was off:
	//either by 90 degrees, or the hand was flipped or it was sideways
	jointPositions = flipJointPositions(jointPositions)

	//rotating around the y axis again (this is for accuracy)
	jointPositions = rotateJointPositionsOnAxis(jointPositions, 'Y');

	return jointPositions;
}

/*
In this function we return the joint positions that can be NULL after the rotation.
*/
function addExtraJointsToPositions(jointPositions)
{
	const newJointPositions = Array(JOINTS_25.length);
	let skipped = 0;

	for(let i = 0; i < newJointPositions.length; i++)
	{
		if(isUndefinedJoint(i)) 
		{
			newJointPositions[i] = undefined;
			skipped++;
		} 
		else 
		{
			newJointPositions[i] = jointPositions[i - skipped];
		}
	}

	return newJointPositions;
}