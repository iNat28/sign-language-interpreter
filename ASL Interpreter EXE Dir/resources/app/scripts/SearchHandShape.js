const shapeCategories = new ShapeCategoriesManager(shapes);

//In this function we check if there is a shape function for the result we got
function checkForShapeFunction(shape, jointPositions)
{
	return shape.shapeFunction?.(jointPositions) ?? shape.name;
}

/*
In this function we get the distance of our hand position from a certain shape.
*/
function getDistanceFromShape(jointPositions, shape)
{
	return shape.jointPositions.reduce(
		function (distance, currentJointPosition, i)
		{
			if( currentJointPosition.length !== undefined && 
				jointPositions[i] !== undefined &&
				jointPositions[i].length > 0 )
			{
				/*  
				The "correct" way to do this is to take the square root
				of this sum. But getting a square root is slow.
				Thankfully we can do just as well by not taking the root.
				*/
				distance += Vector.distanceSquared(currentJointPosition, jointPositions[i]);
			}
		
			return distance;
		}, 
	0);
}

/*
This function searches the default positions we have 
and compares them to the user's hand in order to find the closest result
*/
function getClosestShapeToHand(jointPositions)
{
	let closestShape = undefined;
	let currentShapeDistance = undefined;

	/*
	We get the time in order to make sure the search doesnt take too long 
	and we dont get into an infinite loop
	if we do, we'll be able to exit and continue
	*/
	let searchLoopStartTime = window.performance.now();

	for(const shapeName in shapeCategories.currentShapeCategory)
	{
		currentShapeDistance = getDistanceFromShape(
			jointPositions,
			shapeCategories.getShapeByName(shapeName)
		);

		if(closestShape === undefined || currentShapeDistance < closestShape.distance)
		{
			closestShape = {
				shapeName: shapeName, 
				distance: currentShapeDistance
			};
		}

		//if the search is taking too long
		if(window.performance.now() - searchLoopStartTime > searchLoopDurationLimit)
		{
			console.log("stop")
			break
		}
	}

	if(closestShape === undefined)
	{
		return;
	}

	//Here we get and return the object of the result we got
	return shapeCategories.getShapeByName(closestShape.shapeName);
}

const searchLoopDurationLimit = 10000;

/*
This function calls all of the other function in order to normalize the hand and search for the result.
*/
function searchHandShape(inputJointPositions) 
{
	let closestShape = undefined;
	let jointPositions = undefined;

	jointPositions = normalizeHandJointPositions(inputJointPositions);
	jointPositions = addExtraJointsToPositions(jointPositions);
	
	closestShape = getClosestShapeToHand(jointPositions);
	closestShape = checkForShapeFunction(closestShape, jointPositions);
	
	//after all of the algorithm, we return the object of the result
	return closestShape;
}