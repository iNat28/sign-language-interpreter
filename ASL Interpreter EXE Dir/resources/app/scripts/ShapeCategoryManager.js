class ShapeCategoriesManager {
    constructor(shapes)
    {
        this.shapes = shapes;
        this.shapeCategoryIndex = 0;
        this.shapeCategoryNames = Object.keys(shapes);
    }

    incrementCurrentShapeCategory()
    {
        this.shapeCategoryIndex++;

        if(this.shapeCategoryIndex >= this.shapeCategoryNames.length)
        {
            this.shapeCategoryIndex = 0;
        }
    }

    get currentShapeCategoryName()
    {
        return this.shapeCategoryNames[this.shapeCategoryIndex];
    }

    get currentShapeCategory()
    {
        return this.shapes[this.currentShapeCategoryName];
    }

    getShapeByName(shape)
    {
        return this.currentShapeCategory[shape];
    }
}