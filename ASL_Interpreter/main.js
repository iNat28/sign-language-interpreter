const { app, BrowserWindow, screen } = require('electron');

function createWindow () {
  const { width, height } = screen.getPrimaryDisplay().workAreaSize;
  const win = new BrowserWindow({
    width,
    height,
    title: "ASL Interpreter",
    icon: __dirname + 'icon.ico',
    webPreferences: {
      nodeIntegration: true
    }
  });

  win.loadFile('index.html');
}
app.whenReady().then(createWindow);

app.on('window-all-closed', function() {
  if (process.platform !== 'darwin') {
    app.quit();
  }
})

app.on('activate', function() {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
})