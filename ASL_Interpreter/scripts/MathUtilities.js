const sin = Math.sin;
const cos = Math.cos;

const AXIS = {
    X: 0,
    Y: 1,
    Z: 2
};
Object.freeze(AXIS);

class Vector 
{
	static origin = [0,0,0];
	/*
	we purposefully get the distance squared and leave out the sqrt in order to get the actual distance
	so that we don't waste time.
	Getting a sqrt is very slow.
	*/
	static distanceSquared(vector1, vector2)
	{
		return (
			Math.pow((vector1[AXIS.X] - vector2[AXIS.X]), 2) + 
			Math.pow((vector1[AXIS.Y] - vector2[AXIS.Y]), 2) + 
			Math.pow((vector1[AXIS.Z] - vector2[AXIS.Z]), 2)
		);
	};

	/*
	This function gives us the slope between 2 axi.
	Meaning, we can get the slope between 2 points on any two axi.
	*/
	static slope(position1, position2, axis1, axis2)
	{
		return Math.atan(
			( position1[axis1] - position2[axis1] ) /
			( position1[axis2] - position2[axis2] )
		);
	}
}

class Matrix {
	//Here we implement the subraction of a vector from a matrix.
	static subtract = {
		byVector: function(matrix, vector) {
            return Matrix.mapMatrix(
                matrix, 
                (currentPointValue, i) => currentPointValue - vector[i]
            );
		}
	};
	
	static multiply = {
		//Here we implement the multiplication of a matrix by another matrix.
		byMatrix: function(A, B) {
			// Math error handling
			const matrices = [A, B];
			const cols = matrices.map((item) => item[0].length);

			//making sure there are the same amount of rows and columns.
			if (!matrices.every((item, i) => item.every((row) => row.length === cols[i])))
			{
				return console.error('All rows in a matrix must have equal amount of columns');
			}
			else if (cols[0] !== B.length)
			{
				return console.error('Amount of columns in the 1st matrix must match amount of rows in the 2nd matrix');
			}
			
			// Calculations
			//mapping the new rows and columns and doing the multiplication
			return A.map((rowA) =>
				B[0].map((_, colBIndex) =>
					rowA.reduce((acc, itemA, rowBIndex) => acc + itemA * B[rowBIndex][colBIndex], 0)
				)
			);
		},
		//Here we implement the multiplication of a matrix by a vector.
		byVector: function(matrix, vector) {
            return Matrix.mapMatrix(
                matrix,
                (currentPointValue, i) => currentPointValue * vector[i]
            );
		},
		/*
		Here we implement the multiplication of a matrix by a scaler.
		The scaler being a number.
		Meaning, we multiply the matrix by a certain number to scale the matrix.
		*/
		byScaler: function(matrix, scalar) {
            return Matrix.mapMatrix(
                matrix, 
                (currentPointValue) => currentPointValue * scalar
            );
		}
	};

	//Here we implement rotating a matrix by a certain angle on every axis.
	static rotationMatrix = {
		X: function(angle) {
			return [
				[1,			0,				0			],    
				[0,			cos(angle),		-sin(angle)	],     
				[0,			sin(angle),		cos(angle)	] 
		   ];
		},
	
		Y: function(angle) {
			return [
				[cos(angle),	0,			sin(angle)	],  
				[0,				1,      	0			],   
				[-sin(angle),	0, 			cos(angle)	] 
			];
		},
		
		Z: function(angle) {
			return [
				[cos(angle),	-sin(angle),	0		],  
				[sin(angle),	cos(angle),		0		],   
				[0,				0,				1		]    
			];
		}
    };
    
    static mapMatrix(matrix, func) {
        return matrix.map((point) => point.map(func));
    }
}