const videoElement = document.getElementById("inputVideo");
const videoContainerElement = document.getElementById("videoContainer");
const canvasContext = canvasElement.getContext('2d');
const currentResultOutputElement = document.getElementById('currentResultOutput');
const textOutputElement = document.getElementById('textOutput');

/*
this function gets the hand positions as it is from the camera and converts those positions
into a list of lists for better use.
*/
function convertMediaPipeHandJoints(jointPositions)
{
	return jointPositions.map(joint => [joint.x, joint.y, joint.z]);
}

//here we produce the result and show it
function outputHandShapeResult(handShapeResult)
{
	if(handShapeResult === undefined)
	{
		return;
	}

	//show the result of a single frame
	currentResultOutputElement.innerHTML = handShapeResult;

	//we have a certain number of frames before we can produce and final result
	if(framesManager.currentFrameCount >= framesManager.frameLimit)
	{
		textOutputElement.value += framesManager.mostCommonShape;
		textOutputElement.scrollTop = textOutputElement.scrollHeight;

		framesManager.reset();
	}

	framesManager.addShape(handShapeResult);
}

async function onResults(results) {
	document.body.classList.add('loaded');

	// Draw the overlays.
	canvasContext.save();
	canvasContext.clearRect(0, 0, canvasElement.width, canvasElement.height);
	canvasContext.drawImage(results.image, 0, 0, canvasElement.width, canvasElement.height);
	
	if (results.multiHandLandmarks && results.multiHandedness) {
		const landmarks = results.multiHandLandmarks[0];
		
		//here we only draw the hand tracking if the user control for hand tracking is checked
		if(handTrackingCheckbox.checked)
		{
			drawConnectors(
				canvasContext, landmarks, HAND_CONNECTIONS,
				{color: '#000000'}
			);
			
			drawLandmarks(
				canvasContext, landmarks, 
				{
					color: '#000000',
					fillColor: '#de0030'
				}
			);
		}

		//we convert the hand position into a list (points) of lists (x,y,z positions)
		const positions = convertMediaPipeHandJoints(landmarks);

		//here we call the search function.
		const result = searchHandShape(positions);
		
		outputHandShapeResult(result);
	}

	canvasContext.restore();
}

const hands = new Hands({locateFile: function(file) {
	return `https://cdn.jsdelivr.net/npm/@mediapipe/hands@0.1/${file}`;
}});
hands.onResults(onResults);

hands.setOptions({
	selfieMode: false,
	maxNumHands: 1,
	minDetectionConfidence: 0.5,
	minTrackingConfidence: 0.5
});

/*
Instantiate a camera. We'll feed each frame we receive into the solution.
*/
const camera = new Camera(videoElement, {
	onFrame: async function() {
		await hands.send({image: videoElement});
	},
	width: canvasElement.width,
	height: canvasElement.height
});
camera.start();



//this function acts to keep the ratio of 16/9 of width to height of the window
window.onresize = function() {
	canvasElement.width = videoContainerElement.clientWidth;
	canvasElement.height = videoContainerElement.clientWidth * 9/16;

	if(canvasElement.height > videoContainerElement.clientHeight)
	{
		canvasElement.height = videoContainerElement.clientHeight;
		canvasElement.width = videoContainerElement.clientHeight * 16/9;
	}
}
window.onresize();