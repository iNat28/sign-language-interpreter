//variables for the settings control panel on the GUI
const canvasElement = document.getElementById('outputCanvas');
const framesSliderOutputElement = document.getElementById("framesNumber");
const clearTextButton = document.getElementById("clearTextButton");
const backspaceButton = document.getElementById("backspaceButton");
const switchShapeCategoryButton = document.getElementById("switchShapeCategoryButton");
const pauseButton = document.getElementById("pauseButton");
const copyTextButton = document.getElementById("copyTextButton");
const framesSliderElement = document.getElementById("framesSlider");

const framesSlider = new mdc.slider.MDCSlider(document.querySelector('.mdc-slider'));
const copyTextAlert = new mdc.snackbar.MDCSnackbar(document.querySelector('.mdc-snackbar'));
const handTrackingCheckbox = new mdc.switchControl.MDCSwitch(document.querySelector('.mdc-switch'));
const selfieSwitch = new mdc.switchControl.MDCSwitch(document.querySelectorAll(".mdc-switch")[1]);
const copyTextUndoButton = new mdc.ripple.MDCRipple(document.querySelectorAll('.mdc-button')[1]);

const framesManager = new FramesManager(20);

let oldSliderValue = undefined;
let oldClipboardText = undefined;

//default settings
framesSliderElement.value = framesManager.frameLimit;
framesSliderOutputElement.value = framesManager.frameLimit;
oldSliderValue = framesSliderOutputElement.value;

switchShapeCategoryButton.innerHTML = shapeCategories.currentShapeCategoryName;

//Oninput and onclick functions for the settings

/*
This function is for the slider that controls the number of frames per result
*/
framesSlider.listen("MDCSlider:input", function() {
	const currentSliderValue = framesSlider.getValue();

	framesSliderOutputElement.value = currentSliderValue;
	framesManager.frameLimit = currentSliderValue;
});
/*
This function is for the text box that controls the number of frames per result
*/
framesSliderOutputElement.oninput = function()
{
	if(!isNaN(this.value) && parseInt(this.value) <= framesSliderElement.max && parseInt(this.value) >= framesSliderElement.min)
	{
		framesSlider.setValue(this.value);
		framesSlider.layout();
		
		framesManager.frameLimit = this.value;
		oldSliderValue = this.value;
	}
	else if (this.value != "")
	{
		this.value = oldSliderValue;
	}
	else
	{
		oldSliderValue = "";
	}
}
/*
This function is for the button that clears the text box of results
*/
clearTextButton.onclick = function()
{
	textOutputElement.value = "";
}
/*
This function is for the button that removes the last item from the results
*/
backspaceButton.onclick = function()
{
	if(textOutputElement.value != "")
	{
		textOutputElement.value = textOutputElement.value.slice(0, -1);
	}
}
/*
This function is for the button that pauses the camera feed
*/
pauseButton.onclick = function()
{
	if(this.innerHTML == "Pause")
	{
		camera.video.pause();
		this.innerHTML = "Play";
	}
	else
	{
		camera.video.play();
		this.innerHTML = "Pause";
	}
}
/*
This function is for the switch that is used for selfie mode
*/
selfieSwitch.listen("change", function() {
	canvasElement.classList.toggle('selfie', selfieSwitch.checked);
});

/*
This function is for the button that switches between letters and numbers
*/
switchShapeCategoryButton.onclick = function()
{
    shapeCategories.incrementCurrentShapeCategory();

    this.innerHTML = shapeCategories.currentShapeCategoryName;
}

/*
This function is for the button that allow the user to copy the text inside the results box
*/
copyTextButton.onclick = async function()
{
	await navigator.clipboard.readText()
	.then(text => {
		oldClipboardText = text;
	})
	.catch(err => {
		console.error('Failed to read clipboard contents: ', err);
	});

	await navigator.clipboard.writeText(textOutputElement.value);

	copyTextAlert.labelText = "Copied the text: " + textOutputElement.value;
	copyTextAlert.timeoutMs = 4000;
	copyTextAlert.open();
}

/*
This function is for the undo button we have an the copy text popup msg
*/
copyTextUndoButton.listen("click", async function() {
	await navigator.clipboard.writeText(oldClipboardText);
});