Lil' Oasis

Lil' Oasis is an ASL Interpreter program designed to get the user's hand
using a web camera and interpret the sign being made by the user to english letters or numbers.

Installation:
Make sure that the computer you are using has Node Js and npm installed on it.
If given access to our Gitlab, clone the project straight from Gitlab:
git clone https://gitlab.com/magshimim-markez-2021/06/605/sign-language-interpreter.git

If not given access to our Gitlab, unzip the zipped folder containing the program files.

Usage:
The program folder contains two folders.
One folder with the program files and one folder used for the packaging of the program into an executable.
There are two ways to run the program.

Running through the EXE file:
Double click on the EXE file in the main directory of the program files.

Running through the Command Line:
cd ASL_Interpreter
npm run start

After running the program, wait for the program to start and then lift your hand and start signing.
You may change the program settings using the settings panel on the side of the screen.

Authors:

Tani Feinberg and Yonatan Poch

Acknowledgments:

Team leader - Guy Lorintzi

Mentor - Amit Mor-Yossef

Initial Project Concept\testing - Chana Poch
